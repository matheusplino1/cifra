<?php
	$get = $_GET;
	$post = $_POST;
	$arrParametros = array_merge($get, $post);	
	$oRetorno = new stdClass();
	$oRetorno->erro = 0;
	
	switch($arrParametros['exec']){
		case "salvar_json":
			$json = $arrParametros['json'];			
			$fp = fopen('answer.json', 'w');
			fwrite($fp, $json);
			fclose($fp);

			if(!file_exists('answer.json')){
				$oRetorno->erro = 1;
				$oRetorno->mensagem = 'Ocorreu um erro ao salvar arquivo!';
				break;
			}
				
			$oRetorno->mensagem = 'Arquivo salvo com sucesso.';
			break;

		case "consulta_json":

			if(!file_exists('answer.json')){
				$oRetorno->erro = 1;
				$oRetorno->mensagem = 'Arquivo json Inexistente!';
				break;
			}

			$file = file_get_contents("answer.json");
			$obj = json_decode($file);

			$oRetorno->json = $obj;
			break;

		case "resumo_criptografico":

			if(is_null($arrParametros['decifrado']) || $arrParametros['decifrado'] == ""){
				$oRetorno->erro = 1;
				$oRetorno->mensagem = 'Texto decifrado nulo ou vazio!';
				break;
			}

			echo json_encode(sha1($arrParametros['decifrado']));
			echo json_encode('aaaaa');
			echo json_encode(sha1($arrParametros['decifrado'], true));
			echo json_encode('aaaaa');
			echo json_encode(sha1($arrParametros['decifrado'], false));
			echo json_encode(hash('sha1', $arrParametros['decifrado']));

			$oRetorno->resumo_criptografico = sha1($arrParametros['decifrado'], false);
			break;
	}

	echo json_encode($oRetorno);
?>