<?php
// phpinfo();
?> 
<html>
	<head>
	    <title>Cifra - Formul&aacute;rio</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	    <meta http-equiv="Expires" CONTENT="0">
	    
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	   	<script type="text/javascript" src="sha1.js"></script>  
	   	<script type="text/javascript" src="cifra.js"></script>  
	   	<style type="text/css">
	   		.mg-top-15{
	   			margin-top: 15px;
	   		}
	   	</style>
	</head>
	<body>
		<form enctype="multipart/form-data" method='POST' action='https://api.codenation.dev/v1/challenge/dev-ps/submit-solution?token=aa149ade535a775dda96a95b9824eae4734b1750'>
			<fieldset>				 
				<div class="container">
					<div class='row mg-top-15'>
						<div class='col-md-12'>
							<input type="button" id="consultar" name="consultar" value='Consultar json'/>
						</div>
					</div>
					<div class='row mg-top-15'>
						<div class='col-md-12'>
							<input type="button" id="decifrar" name="decifrar" value='Decifrar Json'/>
						</div>
					</div>					
					<div class='row mg-top-15'>
						<div class='col-md-12'>
							<input type="button" id="resumoCriptografico" name="resumoCriptografico" value='Gerar Resumo SHA1'/>
						</div>
					</div>
					<div class='row mg-top-15'>
						<div class='col-md-12'>
							<label for="arquivo">Arquivo JSON</label>
							<input type="file" id="answer" name="answer"/>
						</div>
					</div>
				</div>	
				<div class="container">
					<div class='row mg-top-15'>
						<div class='col-md-12'>
							<input type='submit' value='Enviar'/>
						</div>
					</div>
				</div>			
			</fieldset>
		</form>
	</body>
	<script>
		document.querySelector('#consultar').addEventListener('click', () => {
			let 
				myHeaders = new Headers(),
				myInit = { method: 'GET',
			               headers: myHeaders,
			               mode: 'cors',
			               cache: 'default' };

			fetch(`https://api.codenation.dev/v1/challenge/dev-ps/generate-data?token=${TOKEN}`).then(response => {
				return response.json();
			}).then(json => {
				salvarJson(json).then(response => alert(response.mensagem));
			});
		});

		document.querySelector('#decifrar').addEventListener('click', () => {
			let data = new FormData();

			data.append('exec', 'consulta_json');

			fetch('./CifraController.php', { 
					method: "POST",
					body: data
			}).then(response => response.json()).then(response => {
				if(!!response.erro){
					alert(response.mensagem);
				} else {
					let json = response.json;
					json.decifrado = decodeString(json.cifrado, json.numero_casas);
					salvarJson(json).then(response => alert(response.mensagem));
				}
			});
		});

		document.querySelector('#resumoCriptografico').addEventListener('click', () => {
			let data = new FormData();

			data.append('exec', 'consulta_json');

			fetch('./CifraController.php', { 
					method: "POST",
					body: data
			}).then(response => response.json()).then(response => {
				if(!!response.erro){
					alert(response.mensagem);
				} else {
					let 
						json = response.json;
					
					json.resumo_criptografico = sha1(json.decifrado);
					salvarJson(json).then(response => alert(response.mensagem));
				}
			});
		});

		function salvarJson(json){
			let data = new FormData();

			data.append('json', JSON.stringify(json));
			data.append('exec', 'salvar_json');

			return fetch('./CifraController.php', { 
					method: "POST",
					body: data
			}).then(response => response.json());
		}
		// alert(decodeString(TEXTO_CIFRADO));
	</script>
</html>