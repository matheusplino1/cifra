const 
	ASCII_A = 97,
	ASCII_Z = 122,	
	TOKEN = 'aa149ade535a775dda96a95b9824eae4734b1750',
	TEXTO_CIFRADO = "xmznmkbqwv (qv lmaqov) qa ikpqmdml vwb epmv bpmzm qa vwbpqvo uwzm bw ill, jcb zibpmz epmv bpmzm qa vwbpqvo uwzm bw bism ieig. ivbwqvm lm aiqvb-mfcxmzg";

function encodeString (str, numeroDeCasas){
	if(typeof str != 'string'){
		alert('Parâmetro deve ser uma string!');
		return false;
	}

	str = str.toLowerCase();
	let strEncoded = '';

	for(let char of str){
		let charCode = char.charCodeAt(0);
		if(!!isAlfa(charCode)){
			if((charCode + numeroDeCasas) <= ASCII_Z){
				charCode = charCode + numeroDeCasas;
			} else {	
				charCode = (ASCII_A + ((charCode + numeroDeCasas) - ASCII_Z) - 1);
			}
		}
		strEncoded += String.fromCharCode(charCode);
	}

	return strEncoded;
}

function decodeString (str, numeroDeCasas){
	if(typeof str != 'string'){
		alert('Parâmetro deve ser uma string!');
		return false;
	}

	str = str.toLowerCase();
	let strDecoded = '';

	for(let char of str){
		let charCode = char.charCodeAt(0);
		if(!!isAlfa(charCode)){
			if((charCode - numeroDeCasas) >= ASCII_A){
				charCode = charCode - numeroDeCasas;
			} else {	
				charCode = (ASCII_Z - (ASCII_A - (charCode - numeroDeCasas)) + 1);
			}
		}
		strDecoded += String.fromCharCode(charCode);
	}

	return strDecoded;
}

function isAlfa(charCode){
	return (charCode >= ASCII_A && charCode <= ASCII_Z);
}